﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
#region Inspector Variables
    [Header("Movement")]
    [SerializeField] float speed = 5f;
    [SerializeField] float sprintSpeed = 8f;
    [SerializeField] float rotationSpeed = 10;
    [SerializeField] float lerpSpeed = 10;

    [Header("Jump")]
    [SerializeField] float jumpeForce = 5f;
    [SerializeField] float secondJumpeForce = 5f;
    [SerializeField] float dJumpActivateGap = 1f;
    [Range(0, 0.5f)] [SerializeField] float groundCheckRadius = 0.2f;
    [SerializeField] LayerMask groundLayers;
    [SerializeField] Transform feetPoint;
    public ParticleSystem t1, t2;
    [SerializeField] ParticleSystem s;
    [SerializeField] float slideDelay;

    [Header("Slide")]
    [SerializeField] Transform slideCheckPos;
    [SerializeField] Collider2D slideCollider;
    [SerializeField] Collider2D standCollider;
    [Range(0, 0.5f)] [SerializeField] float slideCheckRadius = 0.2f;
    [SerializeField] LayerMask slideObstructLayers;

    [Header("Other Necessities")]
    public Animator myAnimator;
    public Transform playerModel;

    private GameObject form1;

    [SerializeField] GameObject form2;

    public UnityEvent OnLanding;

    public bool formTwoActive;
#endregion


    private bool movementLock;
    private PlayerStats stats;
    private Rigidbody2D myRb;
    private Quaternion trunAngle;
    private float horizontalInput;
    private float currentSpeed, airSpeed;
    private bool jump, secondJump, doublJump, sliding, resetSlide, crouching;
    private bool grounded, wasGrounded;

    private void Awake() {
        if(ProgressTracker.instance.gameProgress.formTwo) {
            form1 = playerModel.gameObject;
            form1.SetActive(false);
            form2.SetActive(true);
            myAnimator = form2.GetComponent<Animator>();
            playerModel = form2.transform;
            formTwoActive = true;
        }
    }

    private void Start() {
        myRb = GetComponent<Rigidbody2D>();
        currentSpeed = speed;

        stats = TheFile.ReadFromStatsFile();
        if(stats == null) {
            stats = new PlayerStats();
            TheFile.WriteToStatsFile(stats);
        }

        if(OnLanding == null)
            OnLanding = new UnityEvent();
        
        movementLock = true;
    }

    private void Update() {
        if(!movementLock) {
            horizontalInput = Input.GetAxis("Horizontal");

            // Player's Rotation Update
            if (horizontalInput < 0)
                trunAngle = Quaternion.Euler(new Vector3(playerModel.rotation.x, 270f, playerModel.rotation.z));
            else if (horizontalInput > 0)
                trunAngle = Quaternion.Euler(new Vector3(playerModel.rotation.x, 90f, playerModel.rotation.z));

        #region Jumping
            //If player is not sliding and is on the ground then trigger a jump
            if (grounded && !sliding && (Input.GetKeyDown(KeyCode.Space))) {
                jump = true;
                
                if(stats.jumper) {
                    StopCoroutine(ActivateDoubleJump());
                    StartCoroutine(ActivateDoubleJump());
                }

                myAnimator.ResetTrigger("EndedJump");
                myAnimator.SetTrigger("StartedJump");
                myAnimator.SetBool("Jump", true);
            }

            if(stats.jumper) {
                if(!grounded && doublJump && (Input.GetKeyDown(KeyCode.Space))) {
                    secondJump = true;
                    AudioManager.instance.Thruster();
                    doublJump = false;
                }
            }
        #endregion
            

        #region Sliding
            if(stats.rocketSlider) {
                //Check if player is on the ground, moving in a direction and input slide key
                if(grounded && (Input.GetKey(KeyCode.LeftControl))) {
                    if(!sliding || resetSlide) {
                        //If reset slide is false meaning the player is doing a new slide action so trigger necessary things
                        if(!resetSlide) {
                            myAnimator.ResetTrigger("EndedSlide");
                            myAnimator.SetTrigger("StartedSlide");
                            slideCollider.enabled = true;
                            standCollider.enabled = false;
                            currentSpeed = sprintSpeed;
                            if (s != null) {
                                StopAllCoroutines();
                                StartCoroutine(PlaySpark());
                            }
                            sliding = true;
                        }
                        resetSlide = false;
                    }
                }

                //Cheching if specified circle is overlapping and 
                //storing in variable so that we don't need to call the function twice
                bool phyOverlap = Physics2D.OverlapCircle(new Vector2(slideCheckPos.position.x, slideCheckPos.position.y), slideCheckRadius, slideObstructLayers);
                
                //If the player is sliding but there is no left or right movment 
                //and is not under some object then stop sliding
                if(sliding && horizontalInput == 0) {
                    if(!phyOverlap)
                        EndSlide();

                    if (!crouching) {
                        if(s != null)
                            s.Stop();
                        crouching = true;
                        myAnimator.SetBool("Crouching", true);
                    }
                }

                if(Input.GetKeyUp(KeyCode.LeftControl)) {
                    //If the player is under something then he can't stop sliding else can end the slide
                    if(phyOverlap)
                        resetSlide = true;
                    else
                        EndSlide();
                }

                if (sliding && crouching && horizontalInput != 0)
                {
                    crouching = false;
                    if (s != null) {
                        StopAllCoroutines();
                        StartCoroutine(PlaySpark());
                    }
                    myAnimator.SetBool("Crouching", false);
                    myAnimator.SetTrigger("StartedSlide");
                }
            }
        #endregion
            
            if(stats.sprinter) {
                //If player is on the ground, not sliding and presses the sprint key
                //then set the current speed to sprinting speed
                if(grounded && !sliding && Input.GetKey(KeyCode.LeftShift))
                    currentSpeed = sprintSpeed;
                else if(!sliding)
                    currentSpeed = speed;
            }

        #region Animation
            if (horizontalInput != 0) {
                if (currentSpeed == sprintSpeed){
                    myAnimator.SetBool("Sprinting", true);
                    myAnimator.SetBool("Running", false);
                } else {
                    myAnimator.SetBool("Sprinting", false);
                    myAnimator.SetBool("Running", true);
                }
            } else {
                myAnimator.SetBool("Sprinting", false);
                myAnimator.SetBool("Running", false);
            }
        #endregion
    }

#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.P))
            stats.sprinter = true;
        if(Input.GetKeyDown(KeyCode.O))
            stats.rocketSlider = true;
        if(Input.GetKeyDown(KeyCode.I))
            stats.jumper = true;
        
        if(Input.GetKeyDown(KeyCode.L)) {
            stats.sprinter = true;
            stats.rocketSlider = true;
            stats.jumper = true;
            TheFile.WriteToStatsFile(stats);
        }
        if(Input.GetKeyDown(KeyCode.J)) {
            stats = new PlayerStats();
            TheFile.WriteToStatsFile(stats);
        }
#endif
    }

    private void FixedUpdate() {
        if(!movementLock) {
        #region Physics Overlaps & Ground Checks
            wasGrounded = grounded;
            if(Physics2D.OverlapCircle(new Vector2(feetPoint.position.x, feetPoint.position.y), groundCheckRadius, groundLayers)) {
                grounded = true;

                //If player was not grounded before that means he was in the air
                //so that results in him just landing and calling the on land event
                if(!wasGrounded)
                    OnLanding.Invoke();
            } else {
                grounded = false;

                //If player was grounded before then he just went air bound
                //so we do things that needed to do when player goes air bound
                if (wasGrounded) {
                    airSpeed = currentSpeed;
                    doublJump = true;
                    if(s != null)
                        s.Stop();
                    myAnimator.SetBool("Fall", true);
                }
            }

            //If slide needs to be reset then we check 
            //if player is not under something so that we can reset slide state
            if(resetSlide) {
                if(!Physics2D.OverlapCircle(new Vector2(slideCheckPos.position.x, slideCheckPos.position.y), slideCheckRadius, slideObstructLayers)){
                    EndSlide();
                    resetSlide = false;
                }
            }
        #endregion
        
            Movement();
        }
    }

    //The function that handels all the player movement
    private void Movement() {
        Vector2 targetVelocity;
        
        playerModel.rotation = Quaternion.Slerp(playerModel.rotation, trunAngle, rotationSpeed * Time.deltaTime);

        //Add a force if the jump is triggered
        if(jump) {
            myRb.AddForce(new Vector2(0, jumpeForce), ForceMode2D.Impulse);
            jump = false;
        }

        //Add a force if the double jump is triggered
        if(secondJump) {
            t1?.Play();
            t2?.Play();
            myRb.velocity = new Vector2(myRb.velocity.x, 0);
            myRb.AddForce(new Vector2(0, secondJumpeForce), ForceMode2D.Impulse);
            secondJump = false;
        }

        //If player is on the ground use regular speed else use the air bound speed
        if(grounded)
            targetVelocity = new Vector2(horizontalInput * currentSpeed, myRb.velocity.y);
        else
            targetVelocity = new Vector2(horizontalInput * airSpeed, myRb.velocity.y);
        
        myRb.velocity = Vector2.Lerp(myRb.velocity, targetVelocity, lerpSpeed * Time.deltaTime);
    }

    //Function that will be called when the player is lands on the ground after being air bound
    public void JustLanded() {
        myAnimator.SetTrigger("EndedJump");
		sliding = false;
        myAnimator.SetBool("Jump", false);
        myAnimator.SetBool("Fall", false);
        doublJump = false;
        AudioManager.instance.jumping = false;
    }

    //Function for activating double jump
    private IEnumerator ActivateDoubleJump() {
        yield return new WaitForSeconds(dJumpActivateGap);
        doublJump = true;
    }

    public void LockMovement() {
        myAnimator.SetBool("Jump", false);
        myAnimator.SetBool("Fall", false);
        myAnimator.SetBool("Sprinting", false);
        myAnimator.SetBool("Running", false);
        myAnimator.SetBool("Crouching", false);
        myAnimator.ResetTrigger("StartedJump");
        myAnimator.ResetTrigger("EndedSlide");
        myAnimator.ResetTrigger("StartedSlide");
        myAnimator.SetTrigger("EndedJump");

        horizontalInput = 0;
        myRb.velocity = Vector2.zero;
        movementLock = true;
    }

    public void UnlockMovment() {
        movementLock = false;
    }

    //Function that ends the slide and resets all required values
    private void EndSlide() {
        StopAllCoroutines();
        standCollider.enabled = true;
        slideCollider.enabled = false;
        AudioManager.instance.Stopper();
        myAnimator.SetTrigger("EndedSlide");
        myAnimator.ResetTrigger("StartedSlide");
        myAnimator.SetBool("Crouching", false);
        crouching = false;
        sliding = false;
        if(s != null)
            s.Stop();
        currentSpeed = speed;
    }

    //Function that will read from stats file and update the current player stats
    public void UpdateStats() {
        stats = TheFile.ReadFromStatsFile();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("MoveingPlatform"))
            transform.parent = other.transform;
    }

    private void OnCollisionExit2D(Collision2D other) {
        if(other.gameObject.CompareTag("MoveingPlatform"))
            transform.parent = null;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(feetPoint.position, groundCheckRadius);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(slideCheckPos.position, slideCheckRadius);
    }

    public IEnumerator PlaySpark()
    {
        yield return new WaitForSeconds(slideDelay);

        s.Play();
    }
}