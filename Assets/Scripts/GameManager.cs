﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Text dialogueTextSpace;
    public GameObject dialoguePannel;
    public Player playerScript;
    public Camera2DPlatformer gameCamScript;
    public Fades fadeOut;
    public CheckPoint[] checkPoints;

    private void Awake() {
        if(instance == null)
            instance = this;
        
        playerScript = FindObjectOfType<Player>();
        gameCamScript = FindObjectOfType<Camera2DPlatformer>();
        checkPoints = FindObjectsOfType<CheckPoint>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ResetCheckPoints() {
        for (int i = 0; i < checkPoints.Length; i++)
            checkPoints[i].Deactivate();
    }
}
