﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour
{
    private void StepsEvent() {
        AudioManager.instance.Footstep();
    }

    private void JumpEvent() {
        AudioManager.instance.JumpGrunt();
    }

    private void StartSlideEvent()
    {
        AudioManager.instance.Slide();
    }
}
