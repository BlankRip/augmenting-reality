﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerStats
{
    public bool sprinter;
    public bool jumper;
    public bool rocketSlider;

    public PlayerStats() {
        sprinter = false;
        jumper = false;
        rocketSlider = false;
    }
}
