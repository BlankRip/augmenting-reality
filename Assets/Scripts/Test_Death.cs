﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Death : MonoBehaviour
{
    [SerializeField] float deathTime = 2f;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")) {
            GameManager.instance.playerScript.LockMovement();
            GameManager.instance.playerScript.myAnimator.SetTrigger("Die");
            AudioManager.instance.Death();
            StartCoroutine(TriggereFade());
        }
    }

    IEnumerator TriggereFade() {
        yield return new WaitForSeconds(deathTime);
        GameManager.instance.fadeOut.retry = true;
        GameManager.instance.fadeOut.gameObject.SetActive(true);
    }
}
