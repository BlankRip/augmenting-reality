﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePlatform : MonoBehaviour
{
    [SerializeField] float speed = 1;
    [Range(0,3)] [SerializeField] float smoothing = 0.1f;
    [SerializeField] float switchDistance = 0.2f;
    [SerializeField] float waitTime = 0.5f;
    [SerializeField] float startDelay = 0f;
    [SerializeField] Transform[] wayPoints;

    Vector3 moveTo;
    Vector3 refVel;
    int waypointTracker;
    bool forward;
    bool move;

    private void Start() {
        forward = true;
        move = false;
        refVel = Vector3.zero;
        StartCoroutine(WaitAtPoint(startDelay));

        for (int i = 0; i < wayPoints.Length; i++)
            wayPoints[i].parent = null;
        moveTo = wayPoints[waypointTracker].position;
    }

    private void FixedUpdate() {
        if(move)
            MoveBtwPoints();
    }

    private void MoveBtwPoints() {
        float distance = Vector3.Distance(transform.position, moveTo);
        if (distance <= switchDistance) {
            if(forward) {
                if (waypointTracker < wayPoints.Length - 1)
                    waypointTracker++;
                else {
                    waypointTracker--;
                    forward = move =false;
                    StartCoroutine(WaitAtPoint(waitTime));
                }
            } else {
                if (waypointTracker > 0)
                    waypointTracker--;
                else {
                    waypointTracker++;
                    forward = true;
                    move = false;
                    StartCoroutine(WaitAtPoint(waitTime));
                }
            }

            moveTo = wayPoints[waypointTracker].position;
        }

        transform.position = Vector3.SmoothDamp(transform.position, moveTo, ref refVel, smoothing, speed);
    }

    IEnumerator WaitAtPoint(float timeGap) {
        yield return new WaitForSeconds(timeGap);
        move = true;
    }
}
