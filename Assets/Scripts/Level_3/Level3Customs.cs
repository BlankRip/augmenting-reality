﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3Customs : MonoBehaviour
{
    [SerializeField] ParticleSystem newT1;
    [SerializeField] ParticleSystem newT2;

    private void Start() {
        if(GameManager.instance.playerScript.formTwoActive)
            SwitchTs();
    }

    public void SwitchTs() {
        GameManager.instance.playerScript.t1 = newT1;
        GameManager.instance.playerScript.t2 = newT2;
    }
}
