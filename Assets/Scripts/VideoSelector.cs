﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoSelector : MonoBehaviour
{
    [SerializeField] List<VideoClip> menuBackgrounds;
    [SerializeField] VideoPlayer videoPlayer;

    void Start()
    {
        videoPlayer.clip = menuBackgrounds[Random.Range(0, menuBackgrounds.Count)];
    }
}
