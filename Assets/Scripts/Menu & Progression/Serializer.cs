﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//Class that serializes AKA converts to binary and vice versa
public static class Serializer
{
    //Function that converts any object to binary
    private static byte[] ObjectToByteArray(object obj)
    {
        BinaryFormatter bf = new BinaryFormatter();        //Binary Formatter to convert to binary

        //'using' keyword so that the MemoryStream will be disposed when the scope ends
        using (var ms = new MemoryStream())    //Creating memory stream to use for binary conversion
        {
            bf.Serialize(ms, obj);      //Converting to binary
            return ms.ToArray();        //Making what ever we get to an array of byte s
        }
    }

    private static object ByteArrayToObject(byte[] arrBytes)
    {
        //'using' keyword so that the MemoryStream will be disposed when the scope ends
        using (MemoryStream memStream = new MemoryStream())
        {
            BinaryFormatter binForm = new BinaryFormatter();   //BinaryFormatter to convert back from binary
            memStream.Write(arrBytes, 0, arrBytes.Length);     //Writing the thing that needs to be converted back from binary onto the memory stream
            memStream.Seek(0, SeekOrigin.Begin);               //Reading from the start fo the thing that needs to be converted back          
            object obj = binForm.Deserialize(memStream);       //COnverting back to readble from binary
            return obj;
        }
    }

    //Function that writes the given object to a file in a given path
    public static void WriteToFile(object obj, string path, string fileName)
    {
        byte[] objByteArray = Serializer.ObjectToByteArray(obj);      //The object as binary in a byte array

        //If path does not exist it will creat the folders on the system
		if (!Directory.Exists(Path.GetDirectoryName(path)))
		{
			Directory.CreateDirectory(Path.GetDirectoryName(path));
		}

        //Creating or overwriting the required file
		using (BinaryWriter writer = new BinaryWriter(File.Open(path + fileName, FileMode.Create)))
		{
			writer.Write(objByteArray);       //Writing on the file that is opened
		}
    }

    //Function that will read the required file from the specified path and returns it as an object
    public static object ReadFromFile(string path, string fileName)
    {
        object obj;
        //Checks if the file exists
        if (File.Exists(path + fileName))
        {
            obj = Serializer.ByteArrayToObject(File.ReadAllBytes(path + fileName));   //Converting from binary to readable for the file requested
            return obj;
        }
        else
            return null;
    }
}
