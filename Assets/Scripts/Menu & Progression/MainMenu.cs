﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] GameObject menuItems;
    [SerializeField] GameObject levelSelectPanel;
    [SerializeField] GameObject resetQuestion;
    [SerializeField] GameObject credits;
    [SerializeField] GameObject fadeOutPanel;
    [SerializeField] Button continueButton;

    private Progress currentProgress;
    private GameObject currentlyActive;

    private void Start() {
        currentlyActive = menuItems;
        currentProgress = ProgressTracker.instance.gameProgress;
        if (continueButton)
            StartCoroutine(ContinueChecker());
        
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Continue() {
        if(!currentProgress.gameCompleted)
            fadeOutPanel.SetActive(true);
        else
            SwitchScreen(levelSelectPanel);
    }

    public void LevelSelected(int id) {
        ProgressTracker.instance.UpdateCurrentLevel(id);
        fadeOutPanel.SetActive(true);

        PlayerStats stats = TheFile.ReadFromStatsFile();
        switch (id)
        {
            case 1: {
                stats.jumper = stats.sprinter = stats.rocketSlider = false;
                break;
            } case 2: {
                stats.sprinter = true;
                stats.jumper = stats.rocketSlider = false;
                break;
            } case 3: {
                stats.jumper = stats.sprinter = true;
                stats.rocketSlider = false;
                break;
            } case 4: {
                stats.jumper = stats.sprinter = true;
                stats.rocketSlider = false;
                break;
            }
        }
        TheFile.WriteToStatsFile(stats);
    }

    public void NewGame() {
        if(!currentProgress.gameCompleted) {
            if(!currentProgress.continueActive) {
                ProgressTracker.instance.ActivateContinue();
                ProgressTracker.instance.UpdateCurrentLevel(1);
                fadeOutPanel.SetActive(true);
            } else {
                SwitchScreen(resetQuestion);
            }
        } else {
            ProgressTracker.instance.UpdateCurrentLevel(1);
            PlayerStats stats = TheFile.ReadFromStatsFile();
            stats.jumper = stats.sprinter = stats.rocketSlider = false;
            TheFile.WriteToStatsFile(stats);
            fadeOutPanel.SetActive(true);
        }
    }

    public void ResetYes() {
        ProgressTracker.instance.ResetProgres();
        ProgressTracker.instance.ActivateContinue();
        ProgressTracker.instance.UpdateCurrentLevel(1);
        fadeOutPanel.SetActive(true);
    }

    public void Credits() {
        SwitchScreen(credits);
    }

    public void Quit() {
        Application.Quit();
    }

    public void Back() {
        SwitchScreen(menuItems);
    }

    public void SwitchScreen(GameObject screen) {
        currentlyActive.SetActive(false);
        screen.SetActive(true);
        currentlyActive = screen;
    }

    public IEnumerator ContinueChecker()
    {
        yield return new WaitForSeconds(1);

        if (currentProgress.continueActive || currentProgress.gameCompleted)
            continueButton.interactable = true;
        else
            continueButton.interactable = false;
    }
}