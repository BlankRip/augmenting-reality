﻿using UnityEngine;

public static class TheFile
{
    static string savePath;         //The path where the file will be created and saved

#region Player Stats
    static string statsFileName = "PlayerStats.data";  //Name of the player stats file

    //Function that writes data to the player stats file
    public static void WriteToStatsFile(PlayerStats stats) {
        savePath = Application.persistentDataPath + "/data/game/";
        Serializer.WriteToFile(stats, savePath, statsFileName);
    }

    //Function that reads the player stats file and returns the data
    public static PlayerStats ReadFromStatsFile() {
        savePath = Application.persistentDataPath + "/data/game/";
        return (PlayerStats)Serializer.ReadFromFile(savePath, statsFileName);
    }
#endregion

#region Game Progress
    static string progressFileName = "GameProgress.data";  //Name of the player stats file

    //Function that writes data to the game progress file
    public static void WriteToProgressFile(Progress stats) {
        savePath = Application.persistentDataPath + "/data/game/";
        Serializer.WriteToFile(stats, savePath, progressFileName);
    }

    //Function that reads the game progress file and returns the data
    public static Progress ReadFromProgressFile() {
        savePath = Application.persistentDataPath + "/data/game/";
        return (Progress)Serializer.ReadFromFile(savePath, progressFileName);
    }
#endregion
}