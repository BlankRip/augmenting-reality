﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressTracker : MonoBehaviour
{
    public static ProgressTracker instance;
    public Progress gameProgress;

    private void Awake() {
        if(instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(this.gameObject);

        gameProgress = TheFile.ReadFromProgressFile();
        if(gameProgress == null) {
            gameProgress = new Progress();
            TheFile.WriteToProgressFile(gameProgress);
        }
    }

#if UNITY_EDITOR
    private void Update() {
        if(Input.GetKeyDown(KeyCode.K)) {
            ResetProgres();
        }
    }
#endif

    public void ResetProgres() {
        gameProgress = new Progress();
        TheFile.WriteToProgressFile(gameProgress);
        TheFile.WriteToStatsFile(new PlayerStats());
    }

    public void UpdateCurrentLevel(int levelIndex) {
        gameProgress.currentLevelIndex = levelIndex;
        gameProgress.currentCheckPoint = 0;
        gameProgress.formTwo = false;
        TheFile.WriteToProgressFile(gameProgress);
    }

    public void UpdateCurrentCheckPoint(int checkPointId) {
        gameProgress.currentCheckPoint = checkPointId;
        TheFile.WriteToProgressFile(gameProgress);
    }

    public void UnlockFormTwo() {
        gameProgress.formTwo = true;
        TheFile.WriteToProgressFile(gameProgress);
    }

    public void ActivateContinue() {
        if(!gameProgress.gameCompleted) {
            gameProgress.continueActive = true;
            TheFile.WriteToProgressFile(gameProgress);
        }
    }

    public void GameCompleted() {
        gameProgress.gameCompleted = true;
        gameProgress.continueActive = false;
        TheFile.WriteToProgressFile(gameProgress);
    }
}
