﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Progress
{
    public int currentLevelIndex;
    public int currentCheckPoint;
    public bool gameCompleted;
    public bool continueActive;
    public bool formTwo;

    public Progress() {
        currentLevelIndex = 1;
        currentCheckPoint = 0;
        gameCompleted = continueActive = formTwo = false;
    }
}
