﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    //On enabeling panel the game will be paused
    private void OnEnable() {
        Pause();
    }

    //On disabling panel the game will resume
    private void OnDisable() {
        Resume();
    }

    public void ResumeButton() {
        gameObject.SetActive(false);
    }

    public void ReturnToMenu() {
        SceneManager.LoadScene(0);
    }

    public void QuitButton() {
        Application.Quit();
    }

    //Will put the game back to normal time scaling and lock cursor
    private void Resume() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
    }

    // Will stop game time and display cursor for interations
    private void Pause() {
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
