﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwithCamClamps : MonoBehaviour
{
    [SerializeField] bool switchXmin;
    [SerializeField] float newXmin;
    [SerializeField] bool switchXmax;
    [SerializeField] float newXmax;
    [SerializeField] bool switchYmin;
    [SerializeField] float newYmin;
    [SerializeField] bool swithYmax;
    [SerializeField] float newYmax;
    Camera2DPlatformer theCam;

    private void Start() {
        theCam = GameManager.instance.gameCamScript;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player"))
            Switch();
    }

    private void Switch() {
        if(switchXmin)
            theCam.xMin = newXmin;
        if(switchXmax)
            theCam.xMax = newXmax;
        
        if(switchYmin)
            theCam.yMin = newYmin;
        if(swithYmax)
            theCam.yMax = newYmax;
    }
    
}
