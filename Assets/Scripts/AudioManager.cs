﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioSource playerSFX;
    [SerializeField] AudioClip footstep;
    [SerializeField] AudioClip jumpGrunt;
    [SerializeField] AudioClip thruster;
    [SerializeField] AudioClip slide;
    [SerializeField] AudioClip death;
    public bool jumping;

    float initialVolume;

    public static AudioManager instance;

    public void Awake()
    {
        instance = this;
        initialVolume = playerSFX.volume;
    }

    public void Footstep()
    {
        if(!jumping) {
            playerSFX.volume = initialVolume;
            playerSFX.pitch = Random.Range(1f, 3f);
            playerSFX.PlayOneShot(footstep);
        }
    }

    public void JumpGrunt()
    {
        playerSFX.volume = initialVolume;
        playerSFX.pitch = 1f;
        jumping = true;
        playerSFX.PlayOneShot(jumpGrunt);
    }

    public void Thruster()
    {
        playerSFX.PlayOneShot(thruster);
    }

    public void Slide()
    {       
        playerSFX.volume = 0.9f;
        playerSFX.pitch = 1;
        playerSFX.clip = slide;
        playerSFX.loop = true;
        playerSFX.Play();
    }

    public void Stopper()
    {
        playerSFX.Stop();
        playerSFX.loop = false;
    }

    public void Death()
    {
        playerSFX.pitch = 1;
        playerSFX.volume = initialVolume;
        playerSFX.PlayOneShot(death);
    }
}
