﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    [SerializeField] float lookRange = 9;
    [SerializeField] float rotationSpeed = 5;
    Transform player;
    Quaternion initialAngle;
    Quaternion turnAngle;

    private void Start() {
        player = GameManager.instance.playerScript.gameObject.transform;
        initialAngle = transform.rotation;
    }

    private void Update() {
        Vector3 dir = new Vector3(player.position.x, transform.position.y, player.position.z) - transform.position;
        if(dir.sqrMagnitude < lookRange * lookRange)
            turnAngle = Quaternion.LookRotation(dir.normalized, Vector3.up);
        else
            turnAngle = initialAngle;

        transform.rotation = Quaternion.Slerp(transform.rotation, turnAngle, rotationSpeed * Time.deltaTime);
    }
}
