﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Exit : MonoBehaviour
{
    [SerializeField] Transform moveToPos;
    [SerializeField] Transform thePlatform;
    [SerializeField] float speed = 4;
    [Range(0,3)] [SerializeField] float smoothing = 0.5f;
    Vector3 refVel;
    bool move;
    AudioSource source;

    private void Start() {
        source = GetComponent<AudioSource>();
        move = false;
        refVel = Vector3.zero;
        moveToPos.parent = null;
    }


    private void FixedUpdate() {
        if(move)
            transform.position = Vector3.SmoothDamp(transform.position, moveToPos.position, ref refVel, smoothing, speed);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")) {
            source.PlayOneShot(source.clip);
            GameManager.instance.playerScript.LockMovement();
            other.transform.parent = thePlatform;
            move = true;
        }
    }
}
