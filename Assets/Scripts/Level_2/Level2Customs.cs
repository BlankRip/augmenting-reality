﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Customs : MonoBehaviour
{
    [SerializeField] GameObject shop;
    [SerializeField] GameObject theRepeatTrigger;

    public void GoToShopEvent() {
        shop.SetActive(true);
    }
    
    public void RemoveGoToShop() {
        theRepeatTrigger.SetActive(false);
    }
}
