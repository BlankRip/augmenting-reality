﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] int myId;
    [SerializeField] Material activeMat;
    [SerializeField] Material inactiveMat;
    [SerializeField] Renderer renderer;
    bool active;

    private void Start() {
        if(ProgressTracker.instance.gameProgress.currentCheckPoint == myId) {
            GameManager.instance.playerScript.gameObject.transform.position = transform.position;
            Activate(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player") && !active)
            Activate(true);
    }

    private void Activate(bool playAudio) {
        if(playAudio) {
            //Play audio here
        }
        GameManager.instance.ResetCheckPoints();
        renderer.material = activeMat;
        active = true;
        ProgressTracker.instance.UpdateCurrentCheckPoint(myId);
    }

    public void Deactivate() {
        renderer.material = inactiveMat;
        active = false;
    }
}
