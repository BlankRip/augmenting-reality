﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoPause : MonoBehaviour
{
    [SerializeField] GameObject pauseScreen;

    //Checks input to display pausescreen
    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape))
            pauseScreen.SetActive(!pauseScreen.activeSelf);
    }
}