﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogue : MonoBehaviour
{
    [SerializeField] GameObject sequence;
    [SerializeField] bool triggerWhenLocked;
    TypeWriteText myDialogues;
    bool conversationInProgress;
    bool canTrigger;
    

    private void Start() {
        myDialogues = GetComponent<TypeWriteText>();
        conversationInProgress = false;
        canTrigger = true;
        if(triggerWhenLocked) {
            if(GameManager.instance.playerScript.formTwoActive) {
                canTrigger = false;
            }
        }
    }

    private void Update() {
        //If in a conversation then accept input to continue the dialogues
        if(conversationInProgress && Input.GetKeyDown(KeyCode.E)) {
           conversationInProgress = myDialogues.TextInteraction();
        }
    }

    public void Trigger() {
        sequence.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        //Check if player is triggering a conversation and repeat if the conversation is replayable
        if(canTrigger && other.CompareTag("Player")) {
            GameManager.instance.playerScript.LockMovement();
            canTrigger = myDialogues.StartDialogue();
            conversationInProgress = true;
        }
    }
}
