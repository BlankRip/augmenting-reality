﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockAbility : MonoBehaviour
{
    public enum UnlockAugment {Nada, Sprinter, Jumper, Slider};

    [SerializeField] UnlockAugment unlock;
    [SerializeField] AudioSource sourceSE;
    [SerializeField] GameObject currentModel, modelToSwitch;

    private void LockMove() {
        GameManager.instance.playerScript.LockMovement();
    }

    private void PlayeSE() {
        sourceSE.Play();
    }

    private void Unlock() {
        SwitchModel();
        UnlockRequired();
    }

    private void Ended() {
        GameManager.instance.playerScript.UnlockMovment();
        ProgressTracker.instance.UnlockFormTwo();
        Destroy(this.gameObject);
    }


    private void UnlockRequired() {
        PlayerStats stats = TheFile.ReadFromStatsFile();
        switch (unlock)
        {
            case UnlockAugment.Sprinter:
                stats.sprinter = true;
                break;
            case UnlockAugment.Jumper:
                stats.jumper = true;
                break;
            case UnlockAugment.Slider:
                stats.rocketSlider = true;
                break;
        }
        
        TheFile.WriteToStatsFile(stats);
        GameManager.instance.playerScript.UpdateStats();
    }

    private void SwitchModel() {
        Destroy(currentModel);
        modelToSwitch.SetActive(true);
        GameManager.instance.playerScript.playerModel = modelToSwitch.transform;
        GameManager.instance.playerScript.myAnimator = modelToSwitch.GetComponent<Animator>();
    }
}
